package controllers;

/**
 * This class, when appealed, make the thread sleep for 1000ms
 * 
 * @author stefan
 *
 */
public class Loading implements Runnable {

	private ListenerForLoading listenerForLoading;

	public void setOnListener(ListenerForLoading lFL) {
		this.listenerForLoading = lFL;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(1 * 1000); // 2s (= 2 * 1k ms)
			listenerForLoading.loadingL();
		} catch (Exception exc) {
			System.out.println(exc.getMessage());
		}
	}
}