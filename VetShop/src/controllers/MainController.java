package controllers;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
//import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
//import main.OnCompleteListener;
import model.Animal;
import model.Appointment;
import util.DatabaseUtil;

public class MainController implements Initializable {

	// members
	@FXML
	private ListView<String> listView;
	@FXML
	public ImageView imageView;
	@FXML
	private Label petName;
	@FXML
	private Label species;
	@FXML
	private Label race;
	@FXML
	private Label age;
	@FXML
	private Label weight;
	@FXML
	private Label ownerName;
	@FXML
	private Label mobile;
	@FXML
	private Label mail;
	@FXML
	private Label medic;
	@FXML
	private Label duration;
	@FXML
	private TextArea history;
	@FXML 
	private Button button;

	/**
	 *  The listView is auto-populated with the names of all animals,
	 *  and in the console there are written all of the animals' names, and the image in the window is
	 *  updated.
	 */
	public void populateMainListView() {

		// setting an image at start, to be more user-friendly
		Image image = new Image("/images/uitzciuletzci.jpg");
		// Image image = new Image("/Gifs/loading.gif");
		imageView.setImage(image);

		// getting the Data Base ready
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();

		// creating a List of type 'Animal' and filling it with values from Data Base
		List<Animal> animalDBList = (List<Animal>) db.animalList();

		// LAMBDA
		// Creating a filling an ObservableList of type String
		AnimalssName animalNamesList = (anim) -> getAnimalName(anim);

		// Prints animals in the Console, using MAP
		Map<Integer, String> mapp = new HashMap<>();
		for (Animal an : animalDBList)
			mapp.put(an.getIdanimal(), an.getName());

		//Printing the animals in the console
		System.out.println("The Animals are:");
		for (int i = 1; i < animalDBList.size() + 1; ++i)
			System.out.println(mapp.get(i));

		// Updating the ListView with animal names from the ObservableList
		// listView.setItems(animalNameasList);
		listView.setItems(animalNamesList.computeAnimalName(animalDBList));
		listView.refresh();
		db.stop(); // close Data Base

	}

	// taking from each Animal-object-type the string name to ObservableList
	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals) {
			names.add(a.getName());
		}
		return names;
	}

	// initializing
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateMainListView();
	
	}
	
	
	//PropertyChange Listener
	PropertyChangeListener propertyChangeListner = new PropertyChangeListener() {
		
		@Override
		
		public void propertyChange(PropertyChangeEvent event) {
		    String property = event.getPropertyName();
		    if ("uci".equals(property)) {
		    	
		    	Image image = new Image("/images/uitzciuletzci.jpg");
				imageView.setImage(image);
				
		    }
		}
		
	};
	
	
	/**
	 *  This function is the 'OnAction' of the button to update informations about the
	 *  selected appointment.
	 *  
	 * @param act
	 */
	public void GetDetails(ActionEvent act) {

		if(history.getText().equals("bla")) {
			System.out.println("Modified text!");
		}
		//propertyChangeListner.equals("bla");
		
		int select_index;
		// gets the index of the selected row in the listView
		select_index = listView.getSelectionModel().getSelectedIndex();

		// setting up the Data Base...
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		// filling 'apps' with a list of Appointments...
		List<Appointment> apps = (List<Appointment>) db.sortAppsPrintReturn();

		// petRace keeps the selected row's Animal Race
		String petRace = apps.get(select_index).getRace();

		// verifies if in listView there are the appointment dates of the initial
		// values: the animals' names, not the appointments
		// Gets the selected string in the ListView
		String check_if_animal = listView.getSelectionModel().getSelectedItem();

		//ADDED HASHSET
		//create a new hashset named "anm"
		HashSet<String> anm = new HashSet<String>();

	    //gets the animal list from Data Base, and the puts the names of animals into HashSet
		List<Animal> animalz = (List<Animal>) db.animalList();
		for (Animal an : animalz)
			anm.add(an.getName());
		
		//Checks if it is equal with any animal name; if yes, don't Get the Details
		// about the appointment!
		if (anm.contains(check_if_animal)) {
			System.out.println(check_if_animal); //writes in the console the animal's name
			Image gif = new Image("/Gifs/loading.gif"); //set the image: a gif
			setGifImg(gif);
			return;

		}
		
		// Check the Race of the current selected row to display the correct pet image
		if (petRace.equalsIgnoreCase("Indian Hedgehog")) {
			Image image = new Image("/images/hedgehog.jpg");
			imageView.setImage(image);
		} else if (petRace.equalsIgnoreCase("Coton de Tulear")) {
			Image image = new Image("/images/Top.jpg");
			imageView.setImage(image);
		} else if (petRace.equalsIgnoreCase("Chihuahua")) {
			Image image = new Image("/images/Chihuahua funny.jpg");
			imageView.setImage(image);
		} else if (petRace.equalsIgnoreCase("Siberian Husky")) {
			Image image = new Image("/images/siberian husky.jpg");
			imageView.setImage(image);
		} else if (petRace.equalsIgnoreCase("Labrador Retriever")) {
			Image image = new Image("/images/Fuego xD.jpg");
			imageView.setImage(image);
		}

//Getting the info from the selected row
		species.setText(apps.get(select_index).getSpecies());
		race.setText(apps.get(select_index).getRace());
		age.setText(Integer.toString(apps.get(select_index).getAge()));
		weight.setText(apps.get(select_index).getWeight());
		petName.setText(apps.get(select_index).getAnimal());

		ownerName.setText(apps.get(select_index).getOwner());
		mobile.setText(apps.get(select_index).getMobile());
		mail.setText(apps.get(select_index).getMail());

		medic.setText(apps.get(select_index).getMedic());
		duration.setText(apps.get(select_index).getLasting());
		history.setText(apps.get(select_index).getPastDiagnosys());

		Listenerr();
	}

	// GENERICS
	public <T> void setGifImg(T gif) {
		imageView.setImage((Image) gif);
	}

	/**
	 *  It writes in the console:
	 *   "(number)Wait for it...
	 *   *waits 1 second*
	 *   (number + 1)Details Succesfully Updated!"
	 *   
	 *   where number is a static int, increasing by 2 every time this class is called.
	 */
	public void Listenerr() {
		// Added LISTENER
		// Using SINGLETON
		singleton sg =singleton.getInstance();
		
		Loading loading = new Loading();
		//this is written in the console instantly ....
		loading.setOnListener(new ListenerForLoading() {
			@Override
			public void loadingL() {
				//writes in the console the number of 'GetDetails' button clicks multiplied by 2
				//it counts the number of waits and successfully updating details
				System.out.print('(' + Integer.toString(sg.getNumber()) + ')');
				System.out.println(sg.loadingDetails);
			}
		});
		
		//....and this, after some time that is in Loading class....
		System.out.print('(' + Integer.toString(sg.getNumber()) + ')');
		System.out.println(sg.loadingDone);

		loading.run();
	}

	/**
	 * This menuItem lists all the Appointments dates in the listView when chosen.
	 * and updates the window and all it's text boxes
	 */
	public void allApps(ActionEvent act) {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Appointment> apps;

		singleton sg =singleton.getInstance();
		
		// apps gets all the Appointments from Data Base
		apps = db.sortAppsPrintReturn();
		ObservableList<String> dates = getAppointmentsDate(apps);

		// listView gets filled with the dates
		listView.setItems(dates);
		listView.refresh();

		listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		db.stop();
	}

	/**
	 *  takes the dates from the List of Appointments in the Observable List
	 *  
	 * @param apps --all the appointments in a List
	 * @return -- all appointments, but of type ObservableList<String>
	 */
	public ObservableList<String> getAppointmentsDate(List<Appointment> apps) {
		ObservableList<String> apDates = FXCollections.observableArrayList();

		//prints in the console when this action is picked
		Date todayd = new Date();
		System.out.println("All Appointments option clicked: " + todayd);

		for (Appointment a : apps) {

			apDates.add(a.getAppointmentsDate());
		}
		return apDates;
	}

	//functor
	interface AnimalssName {
		ObservableList<String> computeAnimalName(List<Animal> anim);
	}

	
	/**
	 * This menuItem lists all the Appointments dates in the future in the listView when chosen,
	 * and updates the window and all it's text boxes
	 * 
	 */
	public void futureApps(ActionEvent act) {

		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Appointment> apps;

		// apps get all the Appointments from Data Base
		apps = db.sortAppsPrintReturn();
		
		ObservableList<String> dates = getFutureAppointmentsDate(apps);

		// listView gets filled with the dates
		listView.setItems(dates);
		listView.refresh();

		listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		db.stop();
	}

	/**
	 *  takes the dates from the List of Appointments in the Observable List
	 *  
	 * @param apps --all the appointments in a List
	 * @return -- all appointments, but of type ObservableList<String>
	 */
	public ObservableList<String> getFutureAppointmentsDate(List<Appointment> apps) {
		ObservableList<String> apDates = FXCollections.observableArrayList();

		Date todayd = new Date();
		System.out.println("Future appointments option clicked at: " + todayd);

		for (Appointment a : apps) {

			if(a.getApDate().after(todayd))
				apDates.add(a.getAppointmentsDate());
		}
		return apDates;
	}

	/**
	 * This menuItem lists all the Appointments dates from the past in the listView when chosen,
	 * and updates the window and all it's text boxes
	 * 
	 */
	public void pastApps(ActionEvent act) {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Appointment> apps;

		// apps get all the Appointments from Data Base
		apps = db.sortAppsPrintReturn();
		
		ObservableList<String> dates = getPastAppointmentsDate(apps);

		// listView gets filled with the dates
		listView.setItems(dates);
		listView.refresh();

		listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		db.stop();
	}

	/**
	 *  takes the dates from the List of Appointments in the Observable List
	 *  
	 * @param apps --all the appointments in a List
	 * @return -- all appointments, but of type ObservableList<String>
	 */
	public ObservableList<String> getPastAppointmentsDate(List<Appointment> apps) {
		ObservableList<String> apDates = FXCollections.observableArrayList();

		Date todayd = new Date();
		System.out.println("Past appointments option clicked at: " + todayd);

		for (Appointment a : apps) {

			if(a.getApDate().before(todayd))
				apDates.add(a.getAppointmentsDate());
		}
		return apDates;
	}
}