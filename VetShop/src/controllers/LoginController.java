package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;

/**
 * At run, it automatically opens the first window, that is: Login Window,
 * where there'll be requested an username and a password to login.
 * 
 * @author stefan
 *
 */
public class LoginController implements Initializable {

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}

	@FXML
	private AnchorPane rootAnchorPane;
	@FXML
	TextField username;

	@FXML
	TextField password;

    @FXML
    AnchorPane bimage;
    
	@FXML //On the click of "Login" button, checks if username and password are correct and if yes, goes into application
	private void loadSecond(ActionEvent event) throws IOException {
		
		
		String checkUsername = "1";
		String checkPassword = "2";
		
	
		//if username and password are correct
		if (username.getText().equals(checkUsername) && password.getText().equals(checkPassword)) {
			rootAnchorPane.setEffect(null); //removes the (yellow-ish) innershadow
			System.out.println("Log In Successful!"); 
			//going into the application
			BorderPane bp = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			rootAnchorPane.getChildren().setAll(bp);
		} else if (password.getText().equals(checkPassword))
			System.out.println("Incorect Username");
		else if (username.getText().equals(checkUsername))
			System.out.println("Incorect Password");
		else
			System.out.println("Incorect Username & Password");

	}

}
