package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Design Pattern, with 2 params: description and loadingDetails,
 * used especially when 'getDetails' button is clicked: it writes in the console these 2 strings.
 * 
 * @author stefan
 *
 */
public class singleton {

	// static variable single_instance of type singleton
	private static singleton single_instance = null;

	public String loadingDetails; // on click
	public String description;

	public String getDescription() {
		return description;
	}

	/**
	 * The description cannot be replaced: there can only be added more to it!
	 * @param description
	 */
	public void setDescription(String description) {
		if (this.description != null) {
			this.description += '\n';
			this.description += description;
		} else
			this.description = description;
	}

	public String loadingDone; // after 1000ms (1s)
	public Date dNow = new Date();
	public SimpleDateFormat ft;
	public static int number;

	// Getters and Setters
	public String getLoadingDetails() {
		loadingDetails = Integer.toString(number);
		ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a");
		return loadingDetails;
	}

	public static int getNumber() {
		return number++;
	}

	public static void setNumber(int number) {
		singleton.number = number;
	}

	public void setLoadingDetails(String loadingDetails) {
		this.loadingDetails = loadingDetails;
	}

	public String getLoadingDone() {
		return loadingDone;
	}

	public void setLoadingDone(String loadingDone) {
		this.loadingDone = loadingDone;
	}

	// private constructor restricted to this class itself
	private singleton() {
		loadingDetails = "Details Succesfully Updated!";
		loadingDone = "Wait for it...";
		number = 0;
	}

	/** static method to create instance of Singleton class
	 * 
	 * @return
	 */
	public static singleton getInstance() {
		if (single_instance == null)
			single_instance = new singleton();

		return single_instance;
	}
}