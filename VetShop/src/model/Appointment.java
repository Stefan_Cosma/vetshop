package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the appointments database table.
 * 
 */
@Entity
@Table(name="appointments")
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idappointments;

	private int age;

	private String animal;

	@Temporal(TemporalType.DATE)
	private Date apDate;

	private String appointmentsDate;

	private String lasting;

	private String mail;

	private String medic;

	private String mobile;

	private String owner;

	@Lob
	private String pastDiagnosys;

	private String race;

	private String species;

	private String weight;

	public Appointment() {
	}

	public int getIdappointments() {
		return this.idappointments;
	}

	public void setIdappointments(int idappointments) {
		this.idappointments = idappointments;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAnimal() {
		return this.animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}

	public Date getApDate() {
		return this.apDate;
	}

	public void setApDate(Date apDate) {
		this.apDate = apDate;
	}

	public String getAppointmentsDate() {
		return this.appointmentsDate;
	}

	public void setAppointmentsDate(String appointmentsDate) {
		this.appointmentsDate = appointmentsDate;
	}

	public String getLasting() {
		return this.lasting;
	}

	public void setLasting(String lasting) {
		this.lasting = lasting;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMedic() {
		return this.medic;
	}

	public void setMedic(String medic) {
		this.medic = medic;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPastDiagnosys() {
		return this.pastDiagnosys;
	}

	public void setPastDiagnosys(String pastDiagnosys) {
		this.pastDiagnosys = pastDiagnosys;
	}

	public String getRace() {
		return this.race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getWeight() {
		return this.weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

}