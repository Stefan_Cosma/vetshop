package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the medicalpersonnel database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalpersonnel.findAll", query="SELECT m FROM Medicalpersonnel m")
public class Medicalpersonnel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idMedicalPersonnel;

	private String name;

	public Medicalpersonnel() {
	}

	public int getIdMedicalPersonnel() {
		return this.idMedicalPersonnel;
	}

	public void setIdMedicalPersonnel(int idMedicalPersonnel) {
		this.idMedicalPersonnel = idMedicalPersonnel;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}