package unitTests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import controllers.singleton;

public class singletonTests {

	@Test
	public void checkingSingletonsAutoIncrement() {
		singleton sg = singleton.getInstance(); // sg.number = 0
		sg.getNumber(); // sg.number should auto increment to 1
		sg.getNumber(); // ought to be 2

		Assert.assertTrue(sg.number == 2);
	}

	@Test
	public void checkingLoadingDetailsAndLoadingDoneWorksProperly() {
		singleton sg = singleton.getInstance();

		String t1 = "test1";

		sg.setLoadingDetails(t1);

		sg.getLoadingDetails();

		Assert.assertTrue(sg.getLoadingDetails().equals("2")); // auto increment + 2

		sg.getLoadingDetails();
		sg.getNumber(); // + 1..

		Assert.assertTrue(sg.getLoadingDetails().equals("3")); // .. = 3
	}
	
	
	@Test
	public void checkingLoadingDone() {

		singleton sg = singleton.getInstance();

		String t2 = "te2";

		sg.setLoadingDone(t2);

		Assert.assertTrue(sg.getLoadingDone().equals("te2")); // remains the same

	}
	
	@Test
	public void checkingDescription() {
		
		singleton sg = singleton.getInstance();
		
		String test3 = "Hello";
		sg.setDescription(test3);
		
		Assert.assertTrue(sg.getDescription().equals(test3));

		String test32 = "bro";
		sg.setDescription(test32);
		
		String test322 = "Hello\nbro";
		Assert.assertTrue(sg.getDescription().equals(test322));
		
	}

}
