package unitTests;

import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.Animal;
import model.Appointment;
import model.Medicalpersonnel;
import util.DatabaseUtil;

public class MainClassTests {

	@Test
	public void checkingAnimalSortCorrectness() throws Exception {

		DatabaseUtil DBUtil = new DatabaseUtil();
		DBUtil.setup();
		DBUtil.beginTransaction();
		List<Appointment> apps;

		// apps get all the Appointments from Data Base
		apps = DBUtil.sortAppsPrintReturn();

		Appointment apTemp = null;
		for (Appointment a : apps) {
			if (apTemp == null)
				apTemp = a;
			else {

				// compares two Strings character by character lexicographically and returns a
				// positive number
				// if the first one is smaller than the second one
				int i = (a.getAppointmentsDate()).compareTo(apTemp.getAppointmentsDate());
				Boolean check = false;

				if (i >= 1)
					check = true;

				Assert.assertTrue(check);
			}

		}

		DBUtil.commitTransaction();
		DBUtil.closeEntityManager();
	}

	@Test
	public void checkingAnimalIndexCorrectness() throws Exception {

		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();

		// creating a List of type 'Animal' and filling it with values from Data Base

		List<Animal> animalDBList = (List<Animal>) db.animalList();

		int index = 1;
		for (Animal animalut : animalDBList) {
			// Starting from 1, every animal's index should be the previos animal's one + 1
			if (animalut.getIdanimal() != index++)
				Assert.assertTrue(false);
			// OR Assert.assertFalse(true);

		}

	}
	
	
	@Test
	public void checkingAppointmentsMedicalPersonnelCorrectness() throws Exception {

		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUP();
		dbUtil.startTransaction();

		List<Appointment> apDBList = (List<Appointment>) dbUtil.AppointmentsList();
		List<Medicalpersonnel> mpList = (List<Medicalpersonnel>) dbUtil.MedPersonnelList();

		

		//checks if the dogtor in appointments is any doctor in the medicalpersonnel list 
			for (Appointment ap : apDBList) {
				int ok = 0;
				
				for (Medicalpersonnel mp : mpList)
					if(ap.getMedic().equals(mp.getName()))
						ok = 1;
				
				if(ok == 0)
					Assert.assertTrue(false);
			}
	}

	@Test
	public void checkingAppointmentsIndexCorrectness() throws Exception {

		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUP();
		dbUtil.startTransaction();

		List<Appointment> apDBList = (List<Appointment>) dbUtil.AppointmentsList();

		int index = 1;
		for (Appointment ap : apDBList) {
			// Starting from 1, every appointment's index should be the previos's one + 1
			if (ap.getIdappointments() != index++)
				Assert.assertTrue(false);
			// OR Assert.assertFalse(true);

		}

	}

	@Test
	public void checkingAppointmentsAnimalCorrectness() throws Exception {

		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUP();
		dbUtil.startTransaction();

		List<Appointment> apDBList = (List<Appointment>) dbUtil.AppointmentsList();
		List<Animal> animalDBList = (List<Animal>) dbUtil.animalList();

		//checks if the animal in appointments is any animal in the animal list 
			for (Appointment ap : apDBList) {
				int ok = 0;
					for (Animal animalut : animalDBList) 
					if(ap.getAnimal().equals(animalut.getName()))
						ok = 1;
				
				if(ok == 0)
					Assert.assertTrue(false);
			}
	}
	

	@Test
	public void checkingMedicalPersonnelIndexCorrectness() throws Exception {

		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUP();
		dbUtil.startTransaction();

		List<Medicalpersonnel> mpList = (List<Medicalpersonnel>) dbUtil.MedPersonnelList();

		int index = 1;
		for (Medicalpersonnel mp : mpList)

		{
			// Starting from 1, every person's index should be the previous' one + 1
			if (mp.getIdMedicalPersonnel() != index++)
				Assert.assertTrue(false);
			// OR Assert.assertFalse(true);

		}

	}
}