package unitTests;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import controllers.MainController;
import controllers.singleton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import main.Main;
import model.Animal;
import model.Appointment;
import util.DatabaseUtil;

public class mainControllerTests {

	@Test
	public void thereIsAtleastOnePastAppointment() {

		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Appointment> apps;

		// apps get all the Appointments from Data Base
		apps = db.sortAppsPrintReturn();

		Date todayd = new Date();

		for (Appointment a : apps) {

			if (a.getApDate().before(todayd)) {
				Assert.assertTrue(true);
				break;
			}
		}
	}

	@Test
	public void thereIsAtleastOneFutureAppointment() {

		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.beginTransaction();
		List<Appointment> apps;

		// apps get all the Appointments from Data Base
		apps = db.sortAppsPrintReturn();

		Date todayd = new Date();

		for (Appointment a : apps) {

			if (a.getApDate().before(todayd))
				;
			else {
				Assert.assertTrue(true);
				break;
			}
		}
	}
	
	
//	@Test
//	public void thereAreIndeedAllAppointmentsWhenPickThisMenuItem() {
//		DatabaseUtil db = new DatabaseUtil();
//		db.setup();
//		db.beginTransaction();
//
//		// creating a List of type 'Animal' and filling it with values from Data Base
//
//		List<Animal> animalDBList = (List<Animal>) db.animalList();
//		
//		int index = 1;
//		for(Animal animalut : animalDBList)
//		{
//			//Starting from 1, every animal's index should be the previos animal's one + 1
//			if(animalut.getIdanimal() != index++)
//				Assert.assertTrue(false);
//				//OR Assert.assertFalse(true);
//		}
//	}
	
	

}
