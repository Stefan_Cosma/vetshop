package util;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.Medicalpersonnel;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUP() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("VetShop");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void setup() {
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("VetShop");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void beginTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}

	public void savePersonal(Medicalpersonnel pers) {
		entityManager.persist(pers);
	}

	public void saveAppointment(Appointment prog) {
		entityManager.persist(prog);
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void closeEntityManager() {
		entityManager.close();
	}

	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from vetshop.animal", Animal.class)
				.getResultList();
		for (Animal animal : results) {
			System.out.println("Animal ID is: " + animal.getIdanimal() + ", name: " + animal.getName());
		}
	}

	
	public void printAllDoctorsFromDB() {
		List<Medicalpersonnel> results = entityManager.createNativeQuery("Select * from vetshop.medicalpersonnel", Medicalpersonnel.class)
				.getResultList();
		for (Medicalpersonnel pers : results) {
			System.out.println("Medicalpersonnel ID is: " + pers.getIdMedicalPersonnel() + ", name: " + pers.getName());
		}
	}

	public void printAllAppointmentsFromDB() {
		List<Appointment> results = entityManager.createNativeQuery("Select * from vetshop.appointments", Appointment.class)
				.getResultList();
		for (Appointment appo : results) {
			System.out.println("Appointment ID is: " + appo.getIdappointments() + ", animal: " + appo.getAnimal() + " medic: "
					+ appo.getMedic() + " date: " + appo.getAppointmentsDate() + ", the timed required: " + appo.getLasting());
		}
	}

	public final Animal animalFind(int index) {
		List<Animal> results = entityManager
				.createNativeQuery("Select * from vetshop.animal where idanimal = " + index, Animal.class) //Animal cu a mic ???
				.getResultList();
		return results.get(0);
	}

	public void updateAnimal(int index, String newName) {
		Animal animal = (Animal) entityManager.find(Animal.class, index);
		animal.setName(newName);
	}

	public void deleteAnimal(int index) {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshop.appointments where idappointments = " + index, Appointment.class)
				.getResultList();
		for (Appointment appo : results) {
			entityManager.remove(appo);
		}
		Animal animal = (Animal) entityManager.find(Animal.class, index);
		entityManager.remove(animal);
	}

	public final Medicalpersonnel doctorFind(int index) {
		List<Medicalpersonnel> results = entityManager
				.createNativeQuery("Select * from vetshop.medicalpersonnel where idMedicalPersonnel =" + index, Medicalpersonnel.class)
				.getResultList();
		return results.get(0);
	}

	public void updateDoctor(int index, String newName) {
		Medicalpersonnel pers = (Medicalpersonnel) entityManager.find(Medicalpersonnel.class, index);
		pers.setName(newName);
	}

	public void deleteDoctor(int index) {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshop.appointments where idMedicalPersonnel = " + index, Appointment.class)
				.getResultList();
		for (Appointment app : results) {
			entityManager.remove(app);
		}
		Medicalpersonnel pers = (Medicalpersonnel) entityManager.find(Medicalpersonnel.class, index);
		entityManager.remove(pers);
		// entityManager.getTransaction().commit();
	}

	public final Appointment appointmentFind(int index) {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from petshop.programare where idprogramare =" + index, Appointment.class)
				.getResultList();
		return results.get(0);
	}

	public void deleteAppointment(int index) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		entityManager.remove(appo);
	}

	public void updateAppointmentID(int index, int ID) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setIdappointments(ID);
	}

	public void updateAppointmentDoctor(int index, String med) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setMedic(med);
	}

	public void updateAppointmentDate(int index, String dataa) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setAppointmentsDate(dataa);
	}
	
	public void updateAppointmentAnimal(int index, String an) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setAnimal(an);
	}
	
	public void updateAppointmentLasting(int index, String last) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setLasting(last);
	}

	public void sortPrint() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshop.appointments order by appointmentsDate", Appointment.class).getResultList();
		for (Appointment appo : results) {
			System.out.println("Appointments id: " + appo.getIdappointments() + ", animal: "
					+ appo.getAnimal() + ", personal med: "
					+ appo.getMedic());
		}
	}
	
	public void stop() {
		entityManager.close();
	}

	public List<Animal> animalList() {
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
	
	public List<Medicalpersonnel> MedPersonnelList() {
		List<Medicalpersonnel> MPList = (List<Medicalpersonnel>)entityManager.createQuery("SELECT a FROM Medicalpersonnel a",Medicalpersonnel.class).getResultList();
		return MPList;
	}
	
	public List<Appointment> AppointmentsList() {
		//List<Appointment> APList = (List<Appointment>)entityManager.createQuery("SELECT * FROM appointments",Appointment.class).getResultList();
		//return APList;
		
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshop.appointments order by idappointments", Appointment.class).getResultList();
		return results;
	}

	public List<Appointment> sortAppsPrintReturn() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshop.appointments order by appointmentsDate", Appointment.class).getResultList();
		return results;
	}
}